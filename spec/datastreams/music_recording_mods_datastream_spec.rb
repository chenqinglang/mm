require 'spec_helper'

describe MusicRecordingModsDatastream do
  before(:each) do
    @mods = fixture("music_recording_mods_sample.xml")
    @ds = MusicRecordingModsDatastream.from_xml(@mods)
  end
  
  it "should expose info for tracks with explicit terms and simple proxies" do
    puts @ds.term_values(:person)[0]
    @ds.mods.title_info.main_title.should == ["Track Title"]
  end
end