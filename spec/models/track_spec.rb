require 'spec_helper'

describe Track do
  
  before(:each) do
    @track = Track.new
  end
  
  it "should have the specified datastreams" do
    @track.datastreams.keys.should include("descMetadata")
  end
  
  it "should have the attributes of a track and support update_attributes" do
    attributes_hash = {
      "title" => "track title 1"
    }
    
    @track.update_attributes( attributes_hash )
    @track.title.should == attributes_hash["title"]
  end
end