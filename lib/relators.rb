# encoding: utf-8  
module Relators

  def self.marc
    {
      ""              => "",
      "作曲"      => "http://id.loc.gov/vocabulary/relators/cmp",
      "作词"      => "http://id.loc.gov/vocabulary/relators/lyr",
      "表演者"     => "http://id.loc.gov/vocabulary/relators/prf",
      "指挥"     => "http://id.loc.gov/vocabulary/relators/cnd",
      "注解"     => "http://id.loc.gov/vocabulary/relators/ann",
      "艺术家"        => "http://id.loc.gov/vocabulary/relators/art",
      "作者"        => "http://id.loc.gov/vocabulary/relators/aut",
      "编舞" => "http://id.loc.gov/vocabulary/relators/chr",
      "评论"   => "http://id.loc.gov/vocabulary/relators/cmm",
      "创造者"       => "http://id.loc.gov/vocabulary/relators/cre",
      "描绘"      => "http://id.loc.gov/vocabulary/relators/dpc",
      "导演"      => "http://id.loc.gov/vocabulary/relators/drt",
      "捐赠人"         => "http://id.loc.gov/vocabulary/relators/dnr",
      "编辑"        => "http://id.loc.gov/vocabulary/relators/edt",
      "领奖人"       => "http://id.loc.gov/vocabulary/relators/hnr",
      "主办人"          => "http://id.loc.gov/vocabulary/relators/hst",
      "受访者"   => "http://id.loc.gov/vocabulary/relators/ive",
      "采访者"   => "http://id.loc.gov/vocabulary/relators/ivr",
      "解说员"      => "http://id.loc.gov/vocabulary/relators/nrt",
      "摄影师"  => "http://id.loc.gov/vocabulary/relators/pht",
      "制作人"      => "http://id.loc.gov/vocabulary/relators/pro",
      "接受者"     => "http://id.loc.gov/vocabulary/relators/rcp",
      "演讲人"       => "http://id.loc.gov/vocabulary/relators/spk",
    }
  end

  def self.pbcore
    {
      ""                 => "",
      "presenter"        => "http://pbcore.org/vocabularies/publisherRole#presenter",
      "copyright holder" => "http://pbcore.org/vocabularies/publisherRole#copyright-holder",
      "distributor"      => "http://pbcore.org/vocabularies/publisherRole#distributor",
      "publisher"        => "http://pbcore.org/vocabularies/publisherRole#publisher",
      "release agent"    => "http://pbcore.org/vocabularies/publisherRole#realease-agent",
    }
  end

  def self.lc
    {
      "concept"  => "http://id.loc.gov/authorities/sh2007025014#concept",
    }
  end
  
  def self.type
    [
       '',
       '原生态蒙古族音乐',
       '次生态蒙古族音乐',
       '再生态蒙古族音乐',
    ]
  end
  
  def self.category
    [
      '',
      '民间音乐',
      '宗教音乐',
      '宫廷音乐',
      ]
  end
  
  def self.musicalgenres
    [
      '',
      '民间民歌',
      '民间器乐',
      '说唱音乐',
      '民间歌舞',
      '曲艺音乐',
    ]
  end

  def self.contentnature
    [
      '',
      '图林·道',
      '育林·道',
    ]
  end

  def self.contentsubject
    [
      '',
      '颂歌',
      '赞歌',
'叙事歌',
'出征歌','思乡歌','孤儿歌','思母歌','情歌','悲歌','训导歌','劳动歌','哄养歌','划拳歌','摔跤歌','安魂歌','摇篮歌','无词歌'
    ]
  end
  
  def self.musiccharacteristic
    [
      '',
      '长调（乌日图道）',
      '混合调（贝斯日格道）',
      '短调（宝格尼道）',
      '宣叙调（亚日亚道）',
    ]
  end
  
  def self.colorarea
    [
      '',
      '巴尔虎色彩区',
      '科尔沁色彩区',
      '昭乌达色彩区',
      '察哈尔色彩区',
      '乌拉特色彩区',
      '阿拉善色彩区',
      '卫拉特色彩区',
    ]
  end

  def self.primaryplayingform
    [
      '',
      '独奏乐',
      '合奏乐',
    ]
  end

  def self.secondaryplayingform
    [
      '',
      '拉弦乐',
      '弹拨乐',
      '吹奏乐',
      '口弦乐',
      '鄂尔多斯丝竹乐',
      '科尔沁丝竹乐',
      '锡林郭勒丝竹乐',
    ]
  end
 
  def self.tertiaryplayingform
    [
      '',
      '马头琴曲',
      '潮尔曲',
      '四胡曲',
      '雅托格(蒙古筝)',
      '好毕斯(火不思曲)',
      '三弦曲',
      '托甫秀尔',
      '笛子曲(林布)',
      '冒顿潮尔',
      '口弦琴',
    ]
  end

  def self.form
    [
      '',
      '陶利史诗(降魔传)',
      '好来宝（联词）',
      '乌力格尔（蒙古说书)',
    ]
  end

  def self.expressionform
    [
      '',
      '布里亚特舞',
      '萨布日登舞',
      '鄂尔多斯舞',
      '安代舞',
      '筷子舞',
      '顶碗舞',
    ]
  end

  def self.primarygenres
    [
      '',
      '声乐',
      '器乐',
    ]
  end

  def self.secondarygenres
    [
      '',
'交响曲',
'交响音乐',
'交响诗',
'交响音诗',
'交响音画',
'协奏曲',
'钢琴协奏曲',
'马头琴协奏曲',
'四胡协奏曲',
'套曲',
'独唱曲',
'组曲',
'钢琴组曲',
'前奏曲',
'序曲',
'夜曲',
'幻想曲',
'随想曲',
'狂想曲',
'军乐曲',
'进行曲',
'圆舞曲',
'变奏曲',
'改编曲',
'创意曲',
'叙事曲',
'谐谑曲',
'幽默曲',
'练习曲',
'重奏曲',
'钢琴伴奏',
'管弦乐重奏曲',
'独奏曲',
'管弦乐独奏曲',
'管弦乐曲',
    ]
  end
end
