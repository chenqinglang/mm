# encoding: utf-8
# a Fedora Datastream object containing Mods XML for descMetadata
# datastream in Track content type, defined using ActiveFedora and OM

module Mmdb
  
  class BookModsDatastream < ActiveFedora::NokogiriDatastream
  
  	include Mmdb::MmdbcoreMethods
  	
    #OM terminology mapping for the mods xml
    set_terminology do |t|
      t.root(:path=>"mods", :xmlns=>"http://www.loc.gov/mods/v3", :schema=>"http://www.loc.gov/standards/mods/v3/mods-3-2.xsd")
      t.title_info(:path=>"titleInfo") {
        t.main_title(:index_as=>[:facetable],:path=>"title", :label=>"title")
      }
      # This is a mods:name.  The underscore is purely to avoid namespace conflicts.
      t.name_ {
        t.namePart
        t.role(:ref=>[:role])
      }
      # This is a mods:role, which is used within mods:namePart elements
      t.role {
        t.text(:path=>"roleTerm",:attributes=>{:type=>"text"})
        t.code(:path=>"roleTerm",:attributes=>{:type=>"code"})
      }
      t.person(:ref=>:name, :attributes=>{:type=>"personal"}, :index_as=>[:facetable])
      t.organization(:ref=>:name, :attributes=>{:type=>"corporate"}, :index_as=>[:facetable])
      t.conference(:ref=>:name, :attributes=>{:type=>"conference"}, :index_as=>[:facetable])
      t.abstract
      t.keyword
      t.pubdate
      
      t.title(:proxy=>[:mods, :title_info, :main_title])
      
    end
   # Generates an empty Mmdb Mods (used when you call MmdbModsDatastream.new without passing in existing xml)
    def self.xml_template
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.mods(:version=>"3.3", "xmlns:xlink"=>"http://www.w3.org/1999/xlink",
           "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance",
           "xmlns"=>"http://www.loc.gov/mods/v3",
           "xsi:schemaLocation"=>"http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd") {
             xml.titleInfo {
               xml.title
             }
             xml.abstract
           }
     end
     return builder.doc  
   end
 
   def to_solr(solr_doc=Hash.new)
      super(solr_doc)
      # solr_doc.merge!(extract_person_full_names)
      # solr_doc.merge!(extract_person_organizations)
      solr_doc.merge!(:object_type_facet => "书籍")
      solr_doc.merge!(:format => "book")
      # solr_doc.merge!(:title_t => self.find_by_terms(:main_title).text)
      solr_doc
    end
  end
end