# encoding: utf-8
# a Fedora Datastream object containing Mods XML for descMetadata
# datastream in Track content type, defined using ActiveFedora and OM

module Mmdb
  
  class VideoModsDatastream < ActiveFedora::NokogiriDatastream
  
  	include Mmdb::MmdbcoreMethods
  	
    #OM terminology mapping for the mods xml
    set_terminology do |t|
      t.root(:path=>"mods", :xmlns=>"http://www.loc.gov/mods/v3", :schema=>"http://www.loc.gov/standards/mods/v3/mods-3-2.xsd")
      t.title_info(:path=>"titleInfo") {
        t.main_title(:index_as=>[:facetable],:path=>"title", :label=>"title")
      }
      # This is a mods:name.  The underscore is purely to avoid namespace conflicts.
      t.name_ {
        t.namePart
        t.role(:ref=>[:role])
      }
      # This is a mods:role, which is used within mods:namePart elements
      t.role {
        t.text(:path=>"roleTerm",:attributes=>{:type=>"text"})
        t.code(:path=>"roleTerm",:attributes=>{:type=>"code"})
      }
      t.person(:ref=>:name, :attributes=>{:type=>"personal"}, :index_as=>[:facetable])
      t.organization(:ref=>:name, :attributes=>{:type=>"corporate"}, :index_as=>[:facetable])
      t.conference(:ref=>:name, :attributes=>{:type=>"conference"}, :index_as=>[:facetable])
      t.abstract
      t.genre {
        t.type(:index_as=>[:facetable]) # 类 型 原生、次生、再生
        
        # 原生与次生音乐
        t.category(:index_as=>[:facetable]) # 传统音乐分类：民间音乐、宗教音乐、宫廷音乐
        t.musicalgenres(:index_as=>[:facetable]) # 乐种：民间民歌、民间器乐、说唱音乐、民间歌舞、曲艺音乐
        # 民间民歌
        t.contentnature(:index_as=>[:facetable]) # 内容性质分：1、图林·道；2、育林·道
        t.contentsubject(:index_as=>[:facetable]) # 题材内容分：颂歌、赞歌、叙事歌、出征歌 ...
        t.musiccharacteristic(:index_as=>[:facetable]) # 音乐特点：长调（乌日图道）；混合调（贝斯日格道）
        t.colorarea(:index_as=>[:facetable]) # 音乐色彩区：巴尔虎色彩区、科尔沁色彩区
        # 民间器乐
        t.primaryplayingform(:index_as=>[:facetable]) # 演奏形式: 独奏乐，合奏乐
        t.secondaryplayingform(:index_as=>[:facetable]) # 演奏形式第二层： 拉弦乐、、弹拨乐
        t.tertiaryplayingform(:index_as=>[:facetable]) #  演奏形式第三层 马头琴曲，潮尔曲，四胡曲
        # 民间说唱
        t.form(:index_as=>[:facetable]) # 形式分：包括陶利史诗
        # 民间歌舞
        t.expressionform(:index_as=>[:facetable]) # 表现形式包括：布里亚特舞、萨布日登舞
        
        # 再生音乐
        t.primarygenres(:index_as=>[:facetable])
        t.secondarygenres(:index_as=>[:facetable])
      }
      
      t.keyword
      t.pubdate
      
      t.title(:proxy=>[:mods, :title_info, :main_title])
      
    end
   # Generates an empty Mmdb Mods (used when you call MmdbModsDatastream.new without passing in existing xml)
    def self.xml_template
      builder = Nokogiri::XML::Builder.new do |xml|
        xml.mods(:version=>"3.3", "xmlns:xlink"=>"http://www.w3.org/1999/xlink",
           "xmlns:xsi"=>"http://www.w3.org/2001/XMLSchema-instance",
           "xmlns"=>"http://www.loc.gov/mods/v3",
           "xsi:schemaLocation"=>"http://www.loc.gov/mods/v3 http://www.loc.gov/standards/mods/v3/mods-3-3.xsd") {
             xml.titleInfo {
               xml.title
             }
             xml.abstract
           }
     end
     return builder.doc  
   end
 
   def to_solr(solr_doc=Hash.new)
      super(solr_doc)
      # solr_doc.merge!(extract_person_full_names)
      # solr_doc.merge!(extract_person_organizations)
      solr_doc.merge!(:object_type_facet => "视频")
      solr_doc.merge!(:format => "video")
      # solr_doc.merge!(:title_t => self.find_by_terms(:main_title).text)
      solr_doc
    end
  end
end