# a Fedora object for the Music Score content type

class MusicScore < ActiveFedora::Base
  
  include Hydra::ModelMethods
  include Mmdb::ModelMethods

  has_metadata :name => "descMetadata", :type => Mmdb::MusicScoreModsDatastream
  has_metadata :name => "rightsMetadata", :type => Hydra::Datastream::RightsMetadata

  # adds file_objects methods
  include ActiveFedora::FileManagement

  delegate :title, :to => "descMetadata", :unique=>"true"
  
end
