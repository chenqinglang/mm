# a Fedora object for the Music Recording content type

class MusicRecording < ActiveFedora::Base
  
  include Hydra::ModelMethods
  include Mmdb::ModelMethods

  has_metadata :name => "descMetadata", :type => Mmdb::MusicRecordingModsDatastream
  has_metadata :name => "rightsMetadata", :type => Hydra::Datastream::RightsMetadata

  # adds file_objects methods
  include ActiveFedora::FileManagement

  delegate :title, :to => "descMetadata", :unique=>"true"
  
end
